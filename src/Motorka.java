public class Motorka extends Vozidlo{
    private boolean sideKara;

    public Motorka(String SPZ, String znacka, int pocetKol,boolean sideKara) {
        super(SPZ, znacka, pocetKol);
        this.sideKara = sideKara;
    }

    @Override
    public String toString() {
        return "Motorka{" + super.toString() +
                ", sideKara=" + sideKara +
                '}';
    }
}
