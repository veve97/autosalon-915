import java.util.ArrayList;

public class Autosalon {
    private String nazev;
    ArrayList<Vozidlo> vozidla = new ArrayList<>();

    public Autosalon(String nazev) {
        this.nazev = nazev;
    }

    public void pridat(Vozidlo vozidlo){
        vozidla.add(vozidlo);
    }

    public Vozidlo odebrat(String SPZ){
            Vozidlo nalezeneVozidlo = najdi(SPZ);
            if(nalezeneVozidlo != null){
                vozidla.remove(nalezeneVozidlo);
            }

        return null;
    }

    public Vozidlo najdi(String SPZ){
        for (Vozidlo vozidlo: vozidla
             ) {
            if(SPZ.equals(vozidlo.getSPZ())){
                return vozidlo;
            }

        }
        return null;
    }

    @Override
    public String toString() {
        return nazev + "{" + vozidla +
                '}';
    }
}
