public class Auto extends Vozidlo{
    private char ridicak;

    public Auto(String SPZ, String znacka, int pocetKol, char ridicak) {
        super(SPZ, znacka,pocetKol);
        this.ridicak = ridicak;
    }

    @Override
    public String toString() {
        return "Auto{" + super.toString() +
                ", ridicak=" + ridicak +
                '}';
    }
}
