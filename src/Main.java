public class Main {
    public static void main(String[] args) {
        Auto auto1 = new Auto("AAA","audi",4,'B');
        Motorka moto1 = new Motorka("BBB","Harley",2,false);
        System.out.println(auto1);
        System.out.println(moto1);
        Autosalon salon1 = new Autosalon("Autosalon1");
        System.out.println(salon1);
        salon1.pridat(moto1);
        salon1.pridat(auto1);
        System.out.println(salon1);
        System.out.println(salon1.najdi("AAA"));
        System.out.println(salon1.najdi("CCC"));
        salon1.odebrat("AAA");
        System.out.println(salon1);
    }

}