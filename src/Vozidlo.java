public class Vozidlo {
    private String SPZ;
    private String znacka;
    private int pocetKol;

    public Vozidlo(String SPZ, String znacka, int pocetKol) {
        this.SPZ = SPZ;
        this.znacka = znacka;
        this.pocetKol = pocetKol;
    }
    public Vozidlo(String znacka, int pocetKol) {
        this.znacka = znacka;
        this.pocetKol = pocetKol;
    }

    public String getSPZ() {
        return SPZ;
    }

    public void setSPZ(String SPZ) {
        this.SPZ = SPZ;
    }

    public String getZnacka() {
        return znacka;
    }

    public void setZnacka(String znacka) {
        this.znacka = znacka;
    }

    public int getPocetKol() {
        return pocetKol;
    }

    public void setPocetKol(int pocetKol) {
        this.pocetKol = pocetKol;
    }

    @Override
    public String toString() {
        return
                "SPZ='" + SPZ + '\'' +
                ", znacka='" + znacka + '\'' +
                ", pocetKol=" + pocetKol
                ;
    }
}
